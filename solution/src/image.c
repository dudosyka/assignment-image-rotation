//
// Created by Alex Hatson on 18.10.2023.
//

#include "image.h"
#include <printf.h>
#include <stdlib.h>

#define PIXEL_STRUCT_SIZE sizeof(struct pixel)

static const char* image_status_translated[] = {
        [IMAGE_BAD_HEIGHT] = "bad height provided",
        [IMAGE_BAD_WIDTH] = "bad width provided",
        [IMAGE_OUT_OF_MEMORY] = "out of memory"
};

static uint64_t image_get_pixel_size(struct image const* img) {
    return img->height * img->width;
}

static uint64_t image_calculate_index(struct image const* img, uint64_t x, uint64_t y) {
    return x + y * img->width;
}

uint64_t image_get_size(struct image const* img) {
    return image_get_pixel_size(img) * PIXEL_STRUCT_SIZE;
}

struct maybe_image image_create(uint64_t w, uint64_t h) {
    if (w <= 0)
        return (struct maybe_image) { .status = IMAGE_BAD_WIDTH };
    if (h <= 0)
        return (struct maybe_image) { .status = IMAGE_BAD_HEIGHT };

    struct pixel* data = malloc(w*h*PIXEL_STRUCT_SIZE);
    if (data == NULL)
        return (struct maybe_image) { .status = IMAGE_OUT_OF_MEMORY };

    return (struct maybe_image) {
        .status = IMAGE_OK,
        .img = (struct image) {
            .width = w,
            .height = h,
            .data = data
        }
    };
}

void image_destroy(struct image* img) {
    free(img->data);
    img->width = 0;
    img->height = 0;
}

struct maybe_pixel image_get_pixel(struct image const* img, uint64_t x, uint64_t y) {
    uint64_t index = image_calculate_index(img, x, y);
    if (index < image_get_pixel_size(img))
        return (struct maybe_pixel) {
            .valid = true,
            .value = img->data[index]
        };
    else
        return (struct maybe_pixel) {0};
}

bool image_set_pixel(struct image const* img, uint64_t x, uint64_t y, struct pixel pixel) {
    uint64_t index = image_calculate_index(img, x, y);
    if (index < image_get_pixel_size(img)) {
        img->data[index] = pixel;
        return true;
    } else {
        return false;
    }
}

struct maybe_image image_transform(struct image const source, transformation_function transformation) {
    return transformation(source);
}

const char* translate_image_status(enum image_status status) {
    if (status == IMAGE_OK)
        return "";
    return image_status_translated[status];
}
