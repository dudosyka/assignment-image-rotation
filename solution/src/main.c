#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include "utils.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc >= 3) {
        struct file input = open_file(argv[1], "rb");
        struct file output = open_file(argv[2], "wb");
        if (input.status > 0 || output.status > 0) {
            if (input.status) {
                show_error("input file", translate_read_file_error(input.status));
                int status = input.status;
                return 100 + status;
            }
            if (output.status) {
                show_error("output file", translate_read_file_error(output.status));
                int status = input.status;
                return 200 + status;
            }
        }
        struct image img = {0};
        enum read_status read = from_bmp(input.file, &img);
        if (read > 0) {
            show_error("input file", translate_read_bmp_error(read));
            int status = read;
            return 300 + status;
        }
        close_file(input);
        struct maybe_image rotated = image_transform(img, rotate_90_deg_left);
        image_destroy(&img);
        if (rotated.status != IMAGE_OK) {
            show_error("transformation", translate_image_status(rotated.status));
            int status = rotated.status;
            return 400 + status;
        }
        enum write_status write = to_bmp(output.file, &rotated.img);
        if (write > 0) {
            show_error("output file", translate_write_bmp_error(write));
            int status = write;
            return 500 + status;
        }
        image_destroy(&rotated.img);
        close_file(output);
    } else {
        show_error("", "bad arguments number");
        return 1;
    }
    return 0;
}
