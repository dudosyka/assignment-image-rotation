//
// Created by Alex Hatson on 18.10.2023.
//

#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>

#define BMP_FILE_TYPE 0x4D42
#define PIXEL_STRUCT_SIZE sizeof(struct pixel)
#define BYTE_ALIGNMENT 4
#define BMP_BIT_COUNT (PIXEL_STRUCT_SIZE * 8)


static size_t calculate_padding(const struct image* img) {
    return BYTE_ALIGNMENT - (img->width * PIXEL_STRUCT_SIZE) % BYTE_ALIGNMENT;
}

static enum read_status read_row_from_bmp(struct image* img, FILE* in, size_t row_num, uint16_t padding) {
    for (size_t i = 0; i < img->width; i++) {
        struct pixel* pixel = malloc(PIXEL_STRUCT_SIZE);
        if (pixel == NULL) {
            return READ_OUT_OF_MEMORY;
        }
        size_t read_pixel_result = fread(pixel, PIXEL_STRUCT_SIZE, 1, in);
        if (read_pixel_result != 1) {
            return READ_INVALID_PIXEL;
        }
        image_set_pixel(img, i, row_num, *pixel);
        free(pixel);
    }
    fseek(in, padding, 1);
    return READ_OK;
}

static enum write_status write_row_to_bmp(const struct image* img, FILE* out, size_t row_num, size_t padding) {
    for (size_t i = 0; i < img->width; i++) {
        struct maybe_pixel pixel = image_get_pixel(img, i, row_num);
        if (pixel.valid) {
            size_t write_pixel_result = fwrite(&pixel.value, PIXEL_STRUCT_SIZE, 1, out);
            if (write_pixel_result != 1) {
                return WRITE_ERROR;
            }
        }
    }
    if (padding) {
        fwrite("trash", padding, 1, out);
    }
    return WRITE_OK;
}

static struct bmp_header make_bmp_header(const struct image* img) {
    size_t padding = calculate_padding(img);
    uint32_t pixelSize = img->width * img->height * PIXEL_STRUCT_SIZE + img->height * padding * sizeof(char);
    return (struct bmp_header) {
        .bfType = BMP_FILE_TYPE,
        .bfileSize = pixelSize + sizeof(struct bmp_header),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = BMP_BIT_COUNT,
        .biCompression = 0,
        .biSizeImage = pixelSize,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header;
    size_t read_result = fread(&header, sizeof(struct bmp_header), 1, in);

    if (read_result != 1 || header.bfType != BMP_FILE_TYPE)
        return READ_INVALID_HEADER;

    if (header.biHeight <= 0 || header.biWidth <= 0)
        return READ_INVALID_SIGNATURE;

    if (header.biBitCount != BMP_BIT_COUNT) {
        return READ_INVALID_BITS;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;
    size_t padding = calculate_padding(img);
    img->data = malloc(image_get_size(img));
    if (img->data == NULL) {
        return READ_OUT_OF_MEMORY;
    }

    for (size_t i = 0; i < img->height; i++) {
        enum read_status read = read_row_from_bmp(img, in, i, padding);
        if (read > 0)
            return read;
    }

    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    size_t padding = calculate_padding(img);
    struct bmp_header header = make_bmp_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
        return WRITE_HEADER_ERROR;
    for (size_t i = 0; i < img->height; i++) {
        enum write_status write = write_row_to_bmp(img, out, i, padding);
        if (write > 0)
            return write;
    }
    return WRITE_OK;
}

const char *translate_write_bmp_error(enum write_status write_status) {
    if (write_status == WRITE_OK)
        return "";
    return write_status_translated[write_status];
}

const char *translate_read_bmp_error(enum read_status read_status) {
    if (read_status == READ_OK)
        return "";
    return read_status_translated[read_status];
}

