//
// Created by Alex Hatson on 18.10.2023.
//

#include "rotate.h"
#include <stdlib.h>

struct maybe_image transporate_image(struct image const source) {
    struct maybe_image transporated = image_create(source.height, source.width);

    if (transporated.status != IMAGE_OK)
        return transporated;

    for (size_t row = 0; row < source.height; row++) {
        for (size_t col = 0; col < source.width; col++) {
            struct maybe_pixel source_pixel = image_get_pixel(&source, col, row);
            if (source_pixel.valid) {
                image_set_pixel(&transporated.img, row, col, source_pixel.value);
            }
        }
    }

    return transporated;
}

struct maybe_image reverse_columns(struct image *image) {
    struct maybe_image reversed = image_create(image->width, image->height);

    if (reversed.status != IMAGE_OK)
        return reversed;

    for (size_t col = 0; col < image->width; col++) {
        for (size_t row = 0; row < image->height; row++) {
            struct maybe_pixel image_pixel = image_get_pixel(image, col, row);
            if (image_pixel.valid) {
                image_set_pixel(&reversed.img, image->width - col - 1, row, image_pixel.value);
            }
        }
    }

    return reversed;
}

struct maybe_image rotate_90_deg_left(struct image const source) {
    struct maybe_image transporated = transporate_image(source);

    if (transporated.status != IMAGE_OK)
        return transporated;

    struct maybe_image reversed = reverse_columns(&transporated.img);

    if (reversed.status != IMAGE_OK)
        return reversed;

    image_destroy(&transporated.img);
    return reversed;
}
