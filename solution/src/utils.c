//
// Created by Alex Hatson on 18.10.2023.
//
#include "utils.h"
#include <string.h>
#include <unistd.h>

struct file open_file(char* path, const char* mode) {
    FILE* f = fopen(path, mode);

    if (f == NULL) {
        if (strcmp(mode, "wb") == 0 && access(path, W_OK) == -1) {
            return (struct file) { 0, .status = READ_FILE_CANT_BE_WROTE};
        }
        if (strcmp(mode, "rb") == 0 && access(path, R_OK) == -1) {
            return (struct file) { 0, .status = READ_FILE_CANT_BE_READ};
        }
        return (struct file) { 0, .status = READ_FILE_NOT_FOUND};
    }

    return (struct file) { .file=f, .status=READ_FILE_SUCCESS };
}

void close_file(struct file file) {
    fclose(file.file);
}

const char *translate_read_file_error(enum read_file_status read_file_status) {
    if (read_file_status == READ_FILE_SUCCESS)
        return "";
    return read_file_status_translated[read_file_status];
}

void show_error(const char *prefix, const char *err) {
    fprintf(stderr, "Error: %s %s. \nUsage: image-transformer <input file path> <output file path>", prefix, err);
}
