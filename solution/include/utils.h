//
// Created by Alex Hatson on 18.10.2023.
//

#ifndef IMAGE_TRANSFORMER_UTILS_H
#define IMAGE_TRANSFORMER_UTILS_H

#include "bmp.h"
#include "image.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

    enum read_file_status {
        READ_FILE_SUCCESS = 0,
        READ_FILE_NOT_FOUND,
        READ_FILE_CANT_BE_READ,
        READ_FILE_CANT_BE_WROTE
    };

    static const char* read_file_status_translated[] = {
        [READ_FILE_NOT_FOUND] = "not found",
        [READ_FILE_CANT_BE_READ] = "cant be read",
        [READ_FILE_CANT_BE_WROTE] = "cant be wrote",
    };

    struct file {
        FILE* file;
        enum read_file_status status;
    };

    struct file open_file(char* path, const char* mode);

    void close_file(struct file file);

    uint16_t read_ui16_from_file(FILE* input, size_t start_from);
    uint32_t read_ui32_from_file(FILE* input, size_t start_from);

    void print_uint64(uint64_t);
    void print_pixel(struct pixel);
    void print_bmp_header(struct bmp_header);

    const char* translate_read_file_error(enum read_file_status read_file_status);

    void show_error(const char *prefix, const char *err);
#endif //IMAGE_TRANSFORMER_UTILS_H
