//
// Created by Alex Hatson on 18.10.2023.
//

#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H
#include "image.h"

struct maybe_image rotate_90_deg_left(struct image const source );

#endif //IMAGE_TRANSFORMER_ROTATE_H
