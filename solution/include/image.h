//
// Created by Alex Hatson on 18.10.2023.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include "stdint.h"
#include <stdbool.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel {
    uint8_t r, g, b;
};

struct maybe_pixel {
    bool valid;
    struct pixel value;
};

enum image_status {
    IMAGE_OK = 0,
    IMAGE_BAD_WIDTH,
    IMAGE_BAD_HEIGHT,
    IMAGE_OUT_OF_MEMORY
};

struct maybe_image {
    enum image_status status;
    struct image img;
};

typedef struct maybe_image (transformation_function)(struct image);

struct maybe_image image_create(uint64_t w, uint64_t h);

void image_destroy(struct image* img);

uint64_t image_get_size(struct image const *img);

struct maybe_pixel image_get_pixel(struct image const *img, uint64_t x, uint64_t y);

bool image_set_pixel(struct image const *img, uint64_t x, uint64_t y, struct pixel pixel);

struct maybe_image image_transform(struct image const source, transformation_function transformation);

const char* translate_image_status(enum image_status status);

#endif //IMAGE_TRANSFORMER_IMAGE_H
