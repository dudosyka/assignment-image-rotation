//
// Created by Alex Hatson on 18.10.2023.
//

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H
#include "image.h"
#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)


/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PIXEL,
    READ_OUT_OF_MEMORY
};

static const char* read_status_translated[] = {
        [READ_INVALID_SIGNATURE] = "bad signature",
        [READ_INVALID_BITS] = "invalid bits",
        [READ_INVALID_HEADER] = "bad header",
        [READ_INVALID_PIXEL] = "invalid pixel",
        [READ_OUT_OF_MEMORY] = "out of memory",
};

enum read_status from_bmp( FILE* in, struct image* img );


/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_HEADER_ERROR
};

static const char* write_status_translated[] = {
        [WRITE_ERROR] = "write error",
        [WRITE_HEADER_ERROR] = "write header error"
};

enum write_status to_bmp( FILE* out, struct image const* img );

const char* translate_read_bmp_error(enum read_status read_status);

const char* translate_write_bmp_error(enum write_status write_status);

#endif //IMAGE_TRANSFORMER_BMP_H
